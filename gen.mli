(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type 'a t = Random.State.t -> 'a
include Monad.S1 with type 'a t := 'a t

module RS = Random.State

val int : int -> int t
(** [0..n-1] *)

val int_range : (int * int) -> int t
(** Inclusive *)

(** Including non ASCIIs *)
val char : char t

(** [0-9A-Za-z] *)
val alpha_numeric : char t

val bool : bool t

val string : int t -> char t -> string t
val bytes : int t -> char t -> bytes t
val list : int t -> 'a t -> 'a list t

val elements : 'a list -> 'a t

(** [nelemennts n xs] chooses [n] random elements from [xs].
    The order is preserved.  It fails when [n > List.length xs].
*)
val nelements : int -> 'a list -> 'a list t

(** Randomly choose one of the generators *)
val one_of : 'a t list -> 'a t

val shuffle : 'a list -> 'a list t
val shuffle_inplace : 'a array -> unit t

val tup2 : 'a t -> 'b t -> ('a * 'b) t

(** Run the given generator to generate a value *)
val run : Random.State.t -> 'a t -> 'a

(** [with_gen ?seed gen f] runs [f] with a random seed integer
    and the generated data from [gen] and the seed.

    The seed passed to [f] can be applied to [with_gen] to regenerate the same value.
*)
val with_gen : ?seed:int -> 'a t -> (seed:int -> 'a -> 'b) -> 'b
