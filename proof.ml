module Context_hash : sig
  type t
  val of_bytes : bytes -> t
  val encoding : t Data_encoding.t
  val gen : t Gen.t
end = struct
  type t = bytes (* 32 bytes *)

  let of_bytes s = s

  (* Actual definition is in blake2B.ml of Tezos *)
  let encoding = Data_encoding.Fixed.bytes 32

  let gen = Gen.(bytes (return 32) char)
end

module Proof_types = struct
  (** The type for key segments. *)
  type segment = string

  (** The type for values. *)
  type value = bytes

  let value_encoding : value Data_encoding.t = Data_encoding.bytes

  (** The type for hashes. *)
  type hash = Context_hash.t

  (** The type for (internal) inode proofs.

      These proofs encode large directories into a more efficient tree-like
      structure.

      Invariant are dependent on the backend.

      [length] is the total number of entries in the chidren of the inode.
      It's the size of the "flattened" version of that inode. This is used
      to efficiently implements paginated lists.

      [proofs] have a length of at most [Conf.entries] entries. This list can
      be sparse so every proof is indexed by their position between
      [0 ... (Conf.entries-1)]. For binary trees, this boolean
      index is a segment of the left-right decision proof corresponding
      to the path in that binary tree. *)
  type 'a inode = {length : int; proofs : (int * 'a) list}

  (** The type for inode extenders. *)
  type 'a inode_extender = {length : int; segments : int list; proof : 'a}
  [@@deriving irmin]

  (** The type for inode trees.

      Inodes are optimized representations of trees. Pointers in that trees
      would refer to blinded nodes, nodes or to other inodes.
      Blinded values nor values are not expected to appear directly in
      an inode tree. *)
  type inode_tree =
    | Blinded_inode of hash
    | Inode_values of (segment * tree) list
    | Inode_tree of inode_tree inode
    | Inode_extender of inode_tree inode_extender

  (** The type for compressed and partial Merkle tree proofs.

      [Blinded_value h] is a shallow pointer to a value with hash [h].
      [Value v] is the value [v].

      Tree proofs do not provide any guarantee with the ordering of
      computations. For instance, if two effects commute, they won't be
      distinguishable by this kind of proofs.

      [Blinded_node h] is a shallow pointer to a node having hash [h].

      [Node ls] is a "flat" node containing the list of files [ls]. The length
      of [ls]  is at most [Conf.stable_hash].

      [Inode i] is an optimized representation of a node as a tree. *)
  and tree =
    | Value of value
    | Blinded_value of hash
    | Node of (segment * tree) list
    | Blinded_node of hash
    | Inode of inode_tree inode
    | Extender of inode_tree inode_extender

  (** The type for kinded hashes. *)
  type kinded_hash = [`Value of Context_hash.t | `Node of Context_hash.t]

  (** The type for elements of stream proofs. *)
  type elt =
    | Value of value
    | Node of (segment * kinded_hash) list
    | Inode of hash inode
    | Inode_extender of hash inode_extender

  (** The type for stream proofs. Stream poofs provides stronger ordering
      guarantees as the read effects have to happen in the exact same order and
      they are easier to verify. *)
  type stream = elt Seq.t

  (** The type for proofs of kind ['a].

       A proof [p] proves that the state advanced from [before p] to
       [after p]. [state p]'s hash is [before p], and [state p] contains
       the minimal information for the computation to reach [after p].
       [version p] is the proof version.*)
  type 'a t = {
    version : int;
    before : kinded_hash;
    after : kinded_hash;
    state : 'a;
  }

end

module Enc = struct
  open Proof_types

  let segment_encoding : segment Data_encoding.t =
    (* I understand that a segment here is a file name.
       [bytes] is used For JSON, in the case when it is not UTF-8 *)
    let open Data_encoding in
    conv
      Bytes.unsafe_of_string
      Bytes.unsafe_to_string
      (Bounded.bytes 256 (* 1 byte for the length *))

  let hash_encoding = Context_hash.encoding

  let inode_encoding a =
    let open Data_encoding in
    conv
      (fun {length; proofs} -> ( length, proofs ))
      (fun (length, proofs) -> { length; proofs })
    @@ obj2
      (req "length" int31) (* XXX int31 may not be enough *)
      (req "proofs" (list (tup2 uint8 a))) (* Assuming uint8 covers Conf.entries *)

  let inode_extender_encoding a =
    let open Data_encoding in
    conv
      (fun {length; segments; proof} -> ( length, segments, proof ))
      (fun (length, segments, proof) -> { length; segments; proof })
    @@ obj3
      (req "length" int31) (* XXX int31 may not be enough *)
      (req "segments" (list uint8)) (* Assuming uint8 covers Conf.entries.  Inefficient for binary trees *)
      (req "proof" a)

  (* data-encoding.0.4/test/mu.ml for building mutually recursive data_encodings *)
  let (inode_tree_encoding, tree_encoding) =
    let open Data_encoding in
    let mu_inode_tree_encoding tree_encoding =
      mu "inode_tree" (fun inode_tree_encoding ->
          union [ case ~title: "Blinded_inode" (Tag 0) (obj1 (req "blinded_inode" hash_encoding))
                    (function Blinded_inode h -> Some h | _ -> None)
                    (fun h -> Blinded_inode h);
                  case ~title: "Inode_values" (Tag 1) (obj1 (req "inode_values" (list (tup2 segment_encoding tree_encoding ))))
                    (function Inode_values xs -> Some xs | _ -> None)
                    (fun xs -> Inode_values xs);
                  case ~title: "Inode_tree" (Tag 2) (obj1 (req "inode_tree" (inode_encoding inode_tree_encoding)))
                    (function Inode_tree i -> Some i | _ -> None)
                    (fun i -> Inode_tree i);
                  case ~title: "Inode_extender" (Tag 3) (obj1 (req "inode_extender" (inode_extender_encoding inode_tree_encoding)))
                    (function (Inode_extender i : inode_tree) -> Some i | _ -> None)
                    (fun i -> (Inode_extender i : inode_tree))
                ])
    in
    let mu_tree_encoding =
      mu "tree_encoding" (fun tree_encoding ->
          union [ case ~title: "Value" (Tag 0) (obj1 (req "value" value_encoding))
                    (function (Value v : tree) -> Some v | _ -> None)
                    (fun v -> Value v);
                  case ~title: "Blinded_value" (Tag 1) (obj1 (req "blinded_value" hash_encoding))
                    (function Blinded_value hash -> Some hash | _ -> None)
                    (fun hash -> Blinded_value hash);
                  case ~title: "Node" (Tag 2) (obj1 (req "node" (list (tup2 segment_encoding tree_encoding))))
                    (function (Node sts : tree) -> Some sts | _ -> None)
                    (fun sts -> Node sts);
                  case ~title: "Blinded_node" (Tag 3) (obj1 (req "blinded_node" hash_encoding))
                    (function Blinded_node hash -> Some hash | _ -> None)
                    (fun hash -> Blinded_node hash);
                  case ~title: "Inode" (Tag 4) (obj1 (req "inode" (inode_encoding (mu_inode_tree_encoding tree_encoding))))
                    (function (Inode i: tree) -> Some i | _ -> None)
                    (fun i -> Inode i);
                  case ~title: "Extender" (Tag 5) (obj1 (req "extender" (inode_extender_encoding (mu_inode_tree_encoding tree_encoding))))
                    (function Extender i -> Some i | _ -> None)
                    (fun i -> Extender i);
                ])
    in
    (mu_inode_tree_encoding mu_tree_encoding, mu_tree_encoding)

  let kinded_hash_encoding =
    let open Data_encoding in
    union [ case ~title: "Value" (Tag 0) (obj1 (req "value" Context_hash.encoding))
              (function `Value ch -> Some ch | _ -> None)
              (fun ch -> `Value ch);
            case ~title: "Node" (Tag 1) (obj1 (req "node" Context_hash.encoding))
              (function `Node ch -> Some ch | _ -> None)
              (fun ch -> `Node ch)
          ]

  let elt_encoding =
    let open Data_encoding in
    union [ case ~title: "Value" (Tag 0) (obj1 (req "value" value_encoding))
              (function Value v -> Some v | _ -> None)
              (fun v -> Value v);
            case ~title: "Node" (Tag 1) (obj1 (req "node" (list (tup2 segment_encoding kinded_hash_encoding))))
              (function Node sks -> Some sks | _ -> None)
              (fun sks -> Node sks);
            case ~title: "Inode" (Tag 2) (obj1 (req "inode" (inode_encoding hash_encoding)))
              (function Inode hinode -> Some hinode | _ -> None)
              (fun hinode -> Inode hinode);
            case ~title: "Inode_extender" (Tag 3) (obj1 (req "inode_extender" (inode_extender_encoding hash_encoding)))
              (function Inode_extender e -> Some e | _ -> None)
              (fun e -> Inode_extender e);
          ]

  let encoding a =
    let open Data_encoding in
    conv
      (fun {version; before; after; state} -> (version, before, after, state))
      (fun (version, before, after, state) -> {version; before; after; state})
    @@ obj4
      (req "version" int16)
      (req "before" kinded_hash_encoding)
      (req "after" kinded_hash_encoding)
      (req "state" a)
end

module Generate = struct
  open Proof_types

  let segment : segment Gen.t = Gen.(string (int_range (3, 10)) alpha_numeric)

  let value : value Gen.t = Gen.(bytes (int_range (3, 10)) char)

  let hash = Context_hash.gen

  let inode gen_a =
    let open Gen in
    let* length = int_range (1,10) in
    let+ proofs = list (int 3 >|= (+) 1) (tup2 (int 10) gen_a) in (* no invariant assurance at all :-P *)
    { length; proofs }

  let inode_extender gen_a =
    let open Gen in
    let* length = int_range (1,10) in
    let* segments = list (int 5 >|= (+) 1) (int 4) in
    let+ proof = gen_a in
    { length; segments; proof }

  let rec inode_tree depth rng =
    let open Gen in
    (if depth <= 0 then
       let+ hash = hash in
       Blinded_inode hash
    else
      int 4 >>= function
        | 0 ->
            let+ hash = hash in
            Blinded_inode hash
        | 1 ->
            let+ xs = list (int 3 >|= (+) 1) (tup2 segment (tree (depth - 1))) in
            Inode_values xs
        | 2 ->
            let+ i = inode (inode_tree (depth - 1)) in
            Inode_tree i
        | 3 ->
            let+ i = inode_extender (inode_tree (depth - 1)) in
            (Inode_extender i : inode_tree)
        | _ -> assert false) rng

  and tree depth : tree Gen.t = fun rng ->
    let open Gen in
    (if depth <= 0 then
       int 3 >>= function
         | 0 ->
             let+ v = value in
             (Value v : tree)
         | 1 ->
             let+ h = hash in
             Blinded_value h
         | 2 ->
             let+ h = hash in
             Blinded_node h
         | _ -> assert false
     else
       int 6 >>= function
         | 0 ->
             let+ v = value in
             (Value v : tree)
         | 1 ->
             let+ h = hash in
             Blinded_value h
         | 2 ->
             let+ xs = list (int 4 >|= (+) 1) @@ tup2 segment (tree (depth - 1))
             in
             (Node xs : tree)
         | 3 ->
             let+ h = hash in
             Blinded_node h
         | 4 ->
             let+ i = inode (inode_tree (depth - 1)) in
             (Inode i : tree)
         | 5 ->
             let+ i = inode_extender (inode_tree (depth - 1)) in
             Extender i
         | _ -> assert false) rng
end

let () =
  let open Proof_types in
  let bytes s = Bytes.of_string s in
  let tree_a : tree = Value (bytes "a") in
  let tree_b =
    Extender { length= 10;
               segments= [0;1;0;1];
               proof= Inode_tree {length= 8;
                                  proofs= [0, Blinded_inode (Context_hash.of_bytes (bytes "01234567890123456789012345678901"))]}}
  in
  let data : inode_tree =
    Inode_values ["a", tree_a; "b", tree_b]
  in
  let test data =
    let j = Data_encoding.Json.construct Enc.inode_tree_encoding data in
    Format.eprintf "%a@." Data_encoding.Json.pp j;
    let data' = Data_encoding.Json.destruct Enc.inode_tree_encoding j in
    assert (data = data')
  in
  test data;
  for _ = 0 to 100 do
    Gen.with_gen (Generate.inode_tree 10) @@ fun ~seed:_ inode_tree ->
    test inode_tree;
  done
